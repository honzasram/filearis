# Filearis

Move files from A to B with ease. 

## How it works

Server side of Filearis is watching desired folder for specified files. Once file is created, Filearis will load its content in byte form and send it to desired tentacle with hash and desired final folder.

All of this communication will be secured under HTTPS with JWT token. Client side is HTTPS API awaiting for files.

## Contributors

- Jan Sramek (Idea & Code)
- Beata Handzelova (Name)

## License

Licensed under MIT License.